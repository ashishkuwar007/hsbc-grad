package com.hsbc.da1.dao;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.hsbc.da1.model.Employee;

public class EmployeeSystemDAOImpl implements EmployeeSystemDAO{
	
	private static Set<Employee> set=new HashSet<>();

	@Override
	public Employee enroll(Employee employee) {
		// TODO Auto-generated method stub
		set.add(employee);
		return employee;
	}

	@Override
	public void deleteEmployee(long employeeId) {
		// TODO Auto-generated method stub
		Iterator<Employee> it=set.iterator();
		while(it.hasNext()) {
			Employee updated=it.next();
			if(it.next().getEmployeeId()==employeeId) {
			set.remove(updated);
			}
		}
		
		
	}
		
	

	@Override
	public Set<Employee> fetchAllEmployee() {
		// TODO Auto-generated method stub
		return set;
	}

	@Override
	public Employee fetchById(long employeeId) {
		// TODO Auto-generated method stub
		Iterator<Employee> it=set.iterator();
	int value=0;
	
	
		while(it.hasNext()) {
			
			Employee Account=it.next();
			if(Account.getEmployeeId()==employeeId) {
			return Account;
			}
		}
		return  null;
	}


	@Override
	public Employee fetchByName(String employeeName) {
		// TODO Auto-generated method stub
		Iterator<Employee> it=set.iterator();
	int value=0;
	
	
		while(it.hasNext()) {
			
			Employee Account=it.next();
			if(Account.getEmployeeName()==employeeName) {
			return Account;
			}
		}
		return  null;
	}

	@Override
	public Employee update(long employeeId, Employee employee) {
		// TODO Auto-generated method stub
		Iterator<Employee> it=set.iterator();
		int value=0;
		
		
			while(it.hasNext()) {
				
				Employee Account=it.next();
				if(Account.getEmployeeId()==employeeId) {
				Account=employee;
				return Account;
				}
			}
			return  null;

	}

}
