package com.hsbc.da1.dao;

import java.util.Set;

import com.hsbc.da1.model.Employee;

public interface EmployeeSystemDAO {
	
	public Employee enroll(Employee employee);
	public void deleteEmployee(long employeeId);
	public Set<Employee> fetchAllEmployee();
	public Employee fetchById(long employeeId);
	public Employee fetchByName(String employeeName);
	public Employee update(long employeeId,Employee employee);
	

}
