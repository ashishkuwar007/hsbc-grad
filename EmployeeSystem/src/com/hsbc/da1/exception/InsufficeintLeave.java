package com.hsbc.da1.exception;

public class InsufficeintLeave extends Exception{
	
	
	public InsufficeintLeave(String message) {
		super(message);
	}

	@Override
	public String getMessage() {
		return super.getMessage();
	}
}
