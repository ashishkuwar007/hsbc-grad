package com.hsbc.da1.client;

import com.hsbc.da1.controller.EmployeeSystemController;
import com.hsbc.da1.exception.InsufficeintLeave;
import com.hsbc.da1.exception.MaxLeaveExtended;
import com.hsbc.da1.model.Employee;

public class EmployeeSystemClient {
	public static void main(String[] args) {
		
		EmployeeSystemController controller=new EmployeeSystemController();
		
		Employee ashish=controller.enroll("ashish", 50_000);
		Employee jeetu=controller.enroll("jeetu", 1_00_000);
		
		System.out.println(ashish.getEmployeeId());
              try {
		controller.applyLeave(1001, 5);
              }catch(InsufficeintLeave ex) {
            	  System.out.print(ex.getMessage());
              }catch(MaxLeaveExtended exception) {
            	  System.out.println(exception.getMessage());
              }

		System.out.println(ashish.toString());
		
		
		
	}

}
