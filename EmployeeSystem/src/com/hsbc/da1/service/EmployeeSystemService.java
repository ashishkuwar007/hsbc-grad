package com.hsbc.da1.service;

import java.util.Set;

import com.hsbc.da1.exception.InsufficeintLeave;
import com.hsbc.da1.exception.MaxLeaveExtended;
import com.hsbc.da1.model.Employee;

public interface EmployeeSystemService {

	public Employee enroll(String EmployeeName,long salary);
	public void deleteEmployee(long employeeId);
	public Set<Employee> fetchAllEmployee();
	public Employee fetchById(long employeeId);
	public Employee fetchByName(String employeeName);
	public void applyLeave(long employeeId,int days)throws InsufficeintLeave,MaxLeaveExtended;
	

}
