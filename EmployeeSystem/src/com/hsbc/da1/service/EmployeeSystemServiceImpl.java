package com.hsbc.da1.service;

import java.util.Set;

import com.hsbc.da1.dao.EmployeeSystemDAO;
import com.hsbc.da1.dao.EmployeeSystemDAOImpl;
import com.hsbc.da1.exception.InsufficeintLeave;
import com.hsbc.da1.exception.MaxLeaveExtended;
import com.hsbc.da1.model.Employee;

public class EmployeeSystemServiceImpl implements EmployeeSystemService {
	
	private EmployeeSystemDAO dao=new EmployeeSystemDAOImpl();
		





	@Override
	public Employee enroll(String EmployeeName, long salary) {
		// TODO Auto-generated method stub
		Employee employee =new Employee(EmployeeName, salary);
		return this.dao.enroll(employee);
	}

	@Override
	public void deleteEmployee(long employeeId) {
		// TODO Auto-generated method stub
		this.dao.deleteEmployee(employeeId);
		
	}

	@Override
	public Set<Employee> fetchAllEmployee() {
		// TODO Auto-generated method stub
		return this.dao.fetchAllEmployee();
	}

	@Override
	public Employee fetchById(long employeeId) {
		// TODO Auto-generated method stub
		return this.dao.fetchById(employeeId);
	}

	@Override
	public Employee fetchByName(String employeeName) {
		// TODO Auto-generated method stub
		return this.dao.fetchByName(employeeName);
	}
	public void applyLeave(long employeeId,int days) throws InsufficeintLeave,MaxLeaveExtended{
		if(days<=10) {
		
			Employee employee=this.dao.fetchById(employeeId);
			if(employee.getLeaveBalance()-days>=0) {
				employee.setLeaveBalance(employee.getLeaveBalance()-days);
				Employee updated=this.dao.update(employeeId, employee);
				System.out.println("leave granted for "+days+" days" );
				
			}else
				throw new InsufficeintLeave("please Check Leave Balance ");
		}else {
			throw new MaxLeaveExtended("leave greater than 10 days cant be given");
		}
		
	}

}