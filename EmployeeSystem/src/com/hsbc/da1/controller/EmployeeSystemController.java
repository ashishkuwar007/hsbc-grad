package com.hsbc.da1.controller;

import java.util.Set;

import com.hsbc.da1.exception.InsufficeintLeave;
import com.hsbc.da1.exception.MaxLeaveExtended;
import com.hsbc.da1.model.Employee;
import com.hsbc.da1.service.EmployeeSystemService;
import com.hsbc.da1.service.EmployeeSystemServiceImpl;

public class EmployeeSystemController {
	
	private EmployeeSystemService service=new EmployeeSystemServiceImpl();
	
	public Employee enroll(String EmployeeName,long salary) {
		return this.service.enroll(EmployeeName, salary);
	}
	public void deleteEmployee(long employeeId) {
		this.service.deleteEmployee(employeeId);
	}
	public Set<Employee> fetchAllEmployee(){
		return this.service.fetchAllEmployee();
	}
	public Employee fetchById(long employeeId) {
		return this.service.fetchById(employeeId);
	}
	public Employee fetchByName(String employeeName) {
		return this.service.fetchByName(employeeName);
	}
	public void applyLeave(long employeeId,int days) throws InsufficeintLeave,MaxLeaveExtended{
		this.service.applyLeave(employeeId, days);
	}
	

}
