package com.hsbc.da1.model;

public class Employee {
	private long employeeId;
	private String employeeName;
	private static int Counter=1000;
	private int leaveCounter=40;
	private int LeaveBalance;
	private long Salary;
	
	public Employee(String EmployeeName,long salary) {
		this.employeeId=++Counter;
		this.employeeName=EmployeeName;
		this.LeaveBalance=leaveCounter;
		this.Salary=salary;
	}

	/**
	 * @return the salary
	 */
	public long getSalary() {
		return Salary;
	}

	/**
	 * @param salary the salary to set
	 */
	public void setSalary(long salary) {
		Salary = salary;
	}

	/**
	 * @return the employeeName
	 */
	public String getEmployeeName() {
		return employeeName;
	}

	/**
	 * @param employeeName the employeeName to set
	 */
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	/**
	 * @return the leaveBalance
	 */
	public int getLeaveBalance() {
		return LeaveBalance;
	}

	/**
	 * @param leaveBalance the leaveBalance to set
	 */
	public void setLeaveBalance(int leaveBalance) {
		LeaveBalance = leaveBalance;
	}

	/**
	 * @return the employeeId
	 */
	public long getEmployeeId() {
		return employeeId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (employeeId ^ (employeeId >>> 32));
		result = prime * result + ((employeeName == null) ? 0 : employeeName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Employee)) {
			return false;
		}
		Employee other = (Employee) obj;
		if (employeeId != other.employeeId) {
			return false;
		}
		if (employeeName == null) {
			if (other.employeeName != null) {
				return false;
			}
		} else if (!employeeName.equals(other.employeeName)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", employeeName=" + employeeName + ", LeaveBalance="
				+ LeaveBalance + ", Salary=" + Salary + "]";
	}
	
	

}
