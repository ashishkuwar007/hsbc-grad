

public class Callbyvalref {
    int data=100;

    public static void change(int data){
        data=data+100;
        System.out.println("local change in data :"+data);
    }
    
    public static void main(String[] args) {
        
        
        Callbyvalref local=new Callbyvalref();
        System.out.print("data before local change :"+local.data);
        local.change(500);
        System.out.print("data after local change :"+local.data);

    }

    
}