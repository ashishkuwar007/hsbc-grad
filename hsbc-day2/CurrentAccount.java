

public class CurrentAccount {

  private static long AccountnumberTrack=10_000;

  private String GSTnumber;

  private String Customername;

  private long Accountnumber;
  private long AccountBalance=50_000;
  private Address address;

  private String Businessname;

  

  public CurrentAccount(String CustomerName, String Buisness,String GST){

      this.Customername=CustomerName;
      this.Businessname=Buisness;
      this.Accountnumber= ++AccountnumberTrack;
      this.GSTnumber=GST;
      

  }

  public CurrentAccount(String CustomerName, String Buisness,String GST, Address address){

    this.Customername=CustomerName;
    this.Businessname=Buisness;
    this.Accountnumber= ++AccountnumberTrack;
    this.GSTnumber=GST;
    this.address=address;

}


  public CurrentAccount(String CustomerName, String Buisness,String GST,long accountbalance){

    this.Customername= CustomerName;
    this.Businessname= Buisness;
    this.Accountnumber= ++AccountnumberTrack;
    this.GSTnumber=GST;
    if(accountbalance<50_000)
    {
        System.out.println("account cannot be created with less trhan 50_000 balance");
        return;
    }
    else
    {
    this.AccountBalance=accountbalance;
    }
   


}
public CurrentAccount(String CustomerName, String Buisness,String GST,long accountbalance,Address address){

    this.Customername=CustomerName;
    this.Businessname=Buisness;
    this.Accountnumber= ++AccountnumberTrack;
    this.GSTnumber=GST;
    this.address=address;
    if(accountbalance<50_000)
    {
        System.out.println("account cannot be created with less trhan 50_000 balance");
        return;
    }
    else
    {
    this.AccountBalance=accountbalance;
    }
    

}


  public long getAccountno(){
      return Accountnumber;

  }

  public String getCustomerName(){
    return Customername;

}
  public long withdraw(int amount){
        if(this.AccountBalance-amount<50_000){
            return 0;
        }
        else{
            return this.AccountBalance-amount;
        }

  }
  public long deposit(int amount){
      return AccountBalance+amount;
  }
  public long deposit(int amount,long accountid){
     CurrentAccount user=Fetchaccount.fetchaccount(accountid);
     if(user!=null)
     {
      if(this.AccountBalance-amount<50_000){
                 this.AccountBalance=this.AccountBalance-amount;
                 user.AccountBalance=user.AccountBalance+amount;

      }

     }
     return this.AccountBalance;
}
public Address updateAddress(Address address){
    this.address=address;
    return this.address;

    
}
public Address getaddress(Address address){
  return this.address;
}  

   
}
    
