package com.hsbc.da1.client;

import com.hsbc.da1.controller.ItemController;
import com.hsbc.da1.model.Item;

public class ItemClient {
	public static void main(String[] args) {
		
		ItemController controller=new ItemController();
		
		Item lenovo= controller.createItem("Lenovo Laptop", 50_000);
		
		Item dellMouse= controller.createItem("Dell Gaming mouse", 5000);
		
		System.out.println("lenevo laptop details : id :" +lenovo.getItemId()+ "\n" +lenovo.getItemName()+"\n Price :"+lenovo.getItemPrice());

		System.out.println("Dell mouse details :\n"+dellMouse.getItemName()+"\n Price :"+dellMouse.getItemPrice());
		
		Item updateLenovo=controller.changePrice(lenovo.getItemId(), 40_000);
		
		System.out.println("updated price of lenovo laptop : "+updateLenovo.getItemPrice() );
		
		
	}

}
