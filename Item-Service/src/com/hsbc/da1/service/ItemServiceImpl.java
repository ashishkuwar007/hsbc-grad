package com.hsbc.da1.service;

import com.hsbc.da1.dao.ArraybackedItemDAOImpl;
import com.hsbc.da1.dao.ItemDAO;
import com.hsbc.da1.model.Item;

public class ItemServiceImpl implements ItemService{
	
	ItemDAO dao=new ArraybackedItemDAOImpl();

	@Override
	public Item createItem(String itemName,long itemPrice) {
		Item item=new Item(itemName,itemPrice);
		Item itemCreated=this.dao.createItem(item);
		// TODO Auto-generated method stub
		return itemCreated;
	}

	@Override
	public void deleteItem(long itemId) {
		    this.dao.deleteItem(itemId);
		
		// TODO Auto-generated method stub
		
	}

	@Override
	public Item[] fetchAllItem() {
		
		return this.dao.fetchAllItem();
		// TODO Auto-generated method stub

	}

	@Override
	public Item fetchItemById(long itemId) {
		Item item=this.dao.fetchItemById(itemId);
		// TODO Auto-generated method stub
		return item;
	}

	@Override
	public Item changePrice(long itemId, long itemPrice) {
		Item item=this.dao.fetchItemById(itemId);
		if(item!=null) {
		 item.setItemPrice(itemPrice);
		 
		// TODO Auto-generated method stub
		return this.dao.updateItem(itemId, item);
		}
		return null;
	}
	

}
