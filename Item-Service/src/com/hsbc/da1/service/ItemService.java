package com.hsbc.da1.service;

import com.hsbc.da1.model.Item;

public interface ItemService {
	

	public Item createItem(String itemName,long itemPrice);
	public void deleteItem(long itemId);
	public Item[] fetchAllItem();
	public Item fetchItemById(long itemId);
	public Item changePrice(long itemId,long itemPrice);

}
