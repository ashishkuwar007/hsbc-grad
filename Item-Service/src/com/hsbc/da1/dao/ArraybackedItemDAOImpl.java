package com.hsbc.da1.dao;

import com.hsbc.da1.model.Item;

public class ArraybackedItemDAOImpl implements ItemDAO{
	private static Item[] itemArray=new Item[100];
	private static int itemCount=0;

	@Override
	public Item createItem(Item item) {
		// TODO Auto-generated method stub
		itemArray[itemCount++]=item;
		return item;
		
	}

	@Override
	public void deleteItem(long itemId) {
		// TODO Auto-generated method stub
		for(int index=0;index<itemArray.length;index++) {
			if(itemArray[index].getItemId()==itemId) {
				itemArray[index]=null;
			}
		}
		return;
		
	}

	@Override
	public Item[] fetchAllItem() {
		// TODO Auto-generated method stub
		return itemArray;

	}

	@Override
	public Item fetchItemById(long itemId) {
		
		// TODO Auto-generated method stub
		for(int index=0;index<itemArray.length;index++) {
			if(itemArray[index].getItemId()==itemId) {
				return itemArray[index];
			}
		}
		
		return null;
	}

	@Override
	public Item updateItem(long itemId, Item item) {
		// TODO Auto-generated method stub
		for(int index=0;index<itemArray.length;index++) {
			if(itemArray[index].getItemId()==itemId) {
				itemArray[index]=item;
				return itemArray[index];
			}
		}
	
		return null;
	}

}
