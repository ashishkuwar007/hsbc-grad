package com.hsbc.da1.dao;

import com.hsbc.da1.model.Item;

public interface ItemDAO {
	
	public Item createItem(Item item);
	public void deleteItem(long itemId);
	public Item[] fetchAllItem();
	public Item fetchItemById(long itemId);
	public Item updateItem(long itemId, Item item);
	
	
	
}
