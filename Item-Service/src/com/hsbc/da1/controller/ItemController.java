package com.hsbc.da1.controller;

import com.hsbc.da1.model.Item;
import com.hsbc.da1.service.ItemService;
import com.hsbc.da1.service.ItemServiceImpl;

public class ItemController {
	ItemService service=new ItemServiceImpl();

	public Item createItem(String itemName,long itemPrice) {
		return this.service.createItem(itemName, itemPrice);
		
	}
	public void deleteItem(long itemId) {
		this.service.deleteItem(itemId);
	}
	public Item[] fetchAllItem() {
		return this.service.fetchAllItem();
	}
	public Item fetchItemById(long itemId) {
		return this.service.fetchItemById(itemId);
	}
	public Item changePrice(long itemId, long itemPrice) {
		return this.service.changePrice(itemId, itemPrice);
	}

}
