package com.hsbc.da1.model;

public class Item {

	private String itemName;
	private long itemId;
	private static int counter=1000;
	private long itemPrice;
	
	public Item(String itemName,long itemPrice) {
		this.itemName=itemName;
		this.itemId=++counter;
		this.itemPrice=itemPrice;
		
		
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public long getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(long itemPrice) {
		this.itemPrice = itemPrice;
	}

	public long getItemId() {
		return itemId;
	}
	
	
	
}
