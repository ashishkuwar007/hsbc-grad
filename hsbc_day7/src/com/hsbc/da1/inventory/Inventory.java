package com.hsbc.da1.inventory;

import java.util.ArrayList;
import java.util.List;

public class Inventory{

	public static void main(String[] args) {

		List<Integer> inventory = new ArrayList<>();
		int MAX_SIZE = 5;

		Producer producer = new Producer(inventory, MAX_SIZE);
		Consumer consumer = new Consumer(inventory);

		Thread producerThread = new Thread(producer);
		Thread consumerThread = new Thread(consumer);

		producerThread.start();
		consumerThread.start();

		try {
			producerThread.join();
			consumerThread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

class Producer implements Runnable {
	private List<Integer> inventory;
	private int maxSize;

	private static int counter;

	public Producer(List<Integer> inventory, int maxSize) {
		this.inventory = inventory;
		this.maxSize = maxSize;
	}

	@Override
	public void run() {
		try {
			produce();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private synchronized void produce() throws InterruptedException {
		
			synchronized (inventory) {
				while (true) {
				System.out.println(" SIze of inventory inside the producer "+ this.inventory.size());

				if (this.inventory.size() == this.maxSize) {
					System.out.println(" Inventory is full. Stop producing items");
					inventory.wait();
				}
				Thread.sleep(2000);
				this.inventory.add(counter++);
				System.out.println(" Adding element to the inventory "+ counter);
				inventory.notifyAll();
			}
		}
	}
}

class Consumer implements Runnable {

	private List<Integer> inventory;

	public Consumer(List<Integer> inventory) {
		this.inventory = inventory;
	}

	@Override
	public void run() {
		try {
			consume();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private synchronized void consume() throws InterruptedException {
			synchronized (inventory) {
				while (true) {
				System.out.println(" SIze of inventory "+ this.inventory.size());
				if (this.inventory.isEmpty()) {
					System.out.println(" Inventory is empty: Intimate the producer to start producing");
					inventory.wait();
				}
				Thread.sleep(1000);
				System.out.println("Item + " + inventory.remove(0));
				System.out.println(" Consuming the elements from the inventory");
				inventory.notifyAll();
			}
		}
	}
}
