package com.hsbc.da1.blockingqueue;

import java.util.concurrent.BlockingQueue;
public class Consumer implements Runnable{

	private BlockingQueue<Message> q;

	public Consumer(BlockingQueue<Message> queue) {
		this.q = queue;
	}

	@Override
	public void run() {
		consume();

	}

	private void consume() {
		boolean flag = true;
		while (flag) {
			Message message = null;
			try {
				Thread.sleep(2000);
				message = this.q.take();
				if (message.GetMessage().equals("stop")) {
					flag = false;
				} else {
					System.out.println(" Message is "+ message.GetMessage());
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}


}
