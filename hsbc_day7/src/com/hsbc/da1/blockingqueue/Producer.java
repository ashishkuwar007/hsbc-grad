package com.hsbc.da1.blockingqueue;

import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable {
	
	private BlockingQueue<Message> q;

	public Producer(BlockingQueue<Message> q) {
		this.q=q;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		produceMessage();
		
		}

		private void produceMessage() {
			for( int i = 0; i < 10; i ++ ) {
				Message message = new Message (Math.random()* 100 + "");
				try {
					Thread.sleep(1000);
					if (i == 9) {
						this.q.put(new Message("stop"));
					}else {
						this.q.put(message);
					}
					System.out.println(" Message is put in the queue.... ");
				} catch(InterruptedException e) {
					e.printStackTrace();
				}
			}
			

		
	}

}
