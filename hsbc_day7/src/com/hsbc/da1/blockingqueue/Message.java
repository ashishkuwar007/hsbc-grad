package com.hsbc.da1.blockingqueue;

import java.util.concurrent.BlockingQueue;

public class Message {
	
	private String command;

	public  Message(String command){
		this.command=command;
	}

	public String GetMessage() {
		return this.command;
	}
}
