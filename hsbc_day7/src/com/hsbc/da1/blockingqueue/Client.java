package com.hsbc.da1.blockingqueue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Client {
	
	public static void main(String[] args) {
		BlockingQueue<Message> q=new ArrayBlockingQueue<>(10);
		
		

		Producer producer = new Producer(q);
		Consumer consumer = new Consumer(q);
		
		
		new Thread(producer).start();
		new Thread(consumer).start();
		System.out.println(":: **************  Producer and consumer threads are now started ***************::");
		
//		try {
//			new Thread(producer).join();
//			new Thread(consumer).join();
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		System.out.println(":: **************  Producer and consumer threads are now ended ***************::");

		
	}


	}


