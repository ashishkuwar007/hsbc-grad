package com.hsbc.da1.jdbc;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcDemo {

	public static void main(String[] args) {
		try (Connection connection = DriverManager.getConnection("jdbc:derby://localhost:1527/items", "ashish",
				"password");
				Statement stmt = connection.createStatement();) {
			int count = stmt.executeUpdate("insert into items (name, price) values ('iPad-Pencil', 8000)");
			// count = stmt.executeUpdate("insert into items (name, price) values ('iPhone',
			// 45000)");
			// System.out.println("Number of items inserted :: " + count);

			ResultSet resultSet = stmt.executeQuery("select * from items");
			while (resultSet.next()) {
				System.out.printf("Name of the item %s and the price is %f %n", resultSet.getString(1),
						resultSet.getDouble(2));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
