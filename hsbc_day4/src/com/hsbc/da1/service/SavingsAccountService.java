package com.hsbc.da1.service;

import com.hsbc.da1.model.SavingsAccount;

import java.util.Collection;
import java.util.List;

import javax.security.auth.login.AccountNotFoundException;

import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.Address;

public interface SavingsAccountService {
	
	
	
	public SavingsAccount createSavingsAccount(String customerName,double accountBalance);
	public SavingsAccount createSavingsAccount(String customerName,double accountBalance,Address address);
	public void transfer(long senderId,long reciverId,double amount) throws InsufficientBalanceException,AccountNotFoundException;
		
		
		
	
	public double withdraw(long accountId,double amount) throws InsufficientBalanceException,AccountNotFoundException;
	
	

	public double deposit(long accountId,double amount)throws AccountNotFoundException ;
public SavingsAccount fetchAccountById(long accountId) throws AccountNotFoundException;
	
public Collection<SavingsAccount> fetchAllAccounts();

}