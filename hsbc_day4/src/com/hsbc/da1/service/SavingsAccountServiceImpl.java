package com.hsbc.da1.service;


import java.util.Collection;
import java.util.List;

import javax.security.auth.login.AccountNotFoundException;

import com.hsbc.da1.dao.ArraybackedSavingsAccountDAOImpl;
import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.Address;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.util.SavingsAccountDAOFactory;

public class SavingsAccountServiceImpl implements SavingsAccountService{
	
	private SavingsAccountDAO dao;
	public SavingsAccountServiceImpl(SavingsAccountDAO dao) {
		this.dao=dao;
	}
	
	
	public SavingsAccount createSavingsAccount(String customerName,double accountBalance) {
	  
		SavingsAccount savingsAccount=new SavingsAccount(customerName, accountBalance);
		
		SavingsAccount savingsAccountcreated=this.dao.createSavingsAccount(savingsAccount);
		return savingsAccountcreated;
		
	}
	public SavingsAccount createSavingsAccount(String customerName,double accountBalance,Address address) {
		  
		SavingsAccount savingsAccount=new SavingsAccount(customerName, accountBalance, address);
		
		SavingsAccount savingsAccountcreated=this.dao.createSavingsAccount(savingsAccount);
		return savingsAccountcreated;
		
	}
	public void transfer(long senderId,long reciverId,double amount) throws InsufficientBalanceException , AccountNotFoundException{
		
		SavingsAccount sender=this.dao.fetchAccountById(senderId);
		SavingsAccount reciver=this.dao.fetchAccountById(reciverId);
		double updatedAmount=this.withdraw(senderId, amount);
		if(updatedAmount!=0) {
			
			this.deposit(reciverId, amount);
		}
		
		
		
		
		
	}
	public double withdraw(long accountId,double amount)throws InsufficientBalanceException , AccountNotFoundException{
		
		SavingsAccount savingsAccount=this.dao.fetchAccountById(accountId);
		if(savingsAccount!=null) {
			double balance=savingsAccount.getAccountBalance();
		if(savingsAccount.getAccountBalance()>amount) {
			balance=balance-amount;
			savingsAccount.setAccountBalance(balance);
			this.dao.updateSavingsAccount(accountId, savingsAccount);
			return balance;
		}else {
			throw new InsufficientBalanceException("Thgis amount caanot be transfered since balance is low");
		}
		}
		return 0;
		
	}
	
	

	public double deposit(long accountId,double amount) throws AccountNotFoundException {
		
		SavingsAccount savingsAccount=this.dao.fetchAccountById(accountId);
		if(savingsAccount!=null) {
			double balance=savingsAccount.getAccountBalance();
			balance=balance+amount;
			savingsAccount.setAccountBalance(balance);
			this.dao.updateSavingsAccount(accountId, savingsAccount);
			return balance;
		
		}
		return 0;
	
	}
public SavingsAccount fetchAccountById(long accountId) throws AccountNotFoundException {
		
		
		return this.dao.fetchAccountById(accountId);
	}
	
public Collection<SavingsAccount> fetchAllAccounts() {
	
	
	return this.dao.fetchAllAccounts();
}


}