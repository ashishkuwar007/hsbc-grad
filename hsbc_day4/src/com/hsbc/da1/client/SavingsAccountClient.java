package com.hsbc.da1.client;

import java.util.Scanner;

import javax.security.auth.login.AccountNotFoundException;

import com.hsbc.da1.controller.SavingsAccountController;
import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.service.SavingsAccountService;
import com.hsbc.da1.util.SavingsAccountDAOFactory;
import com.hsbc.da1.util.SavingsAccountServiceFactory;

import com.hsbc.da1.model.Address;

public class SavingsAccountClient {
	public static void main(String[] args) {
		
	

        System.out.println("**************** options********");
        System.out.println(" 1 ---> Array Implementation");
        System.out.println(" 2 ---> ArrayList Implementation");
        System.out.println(" 3---> LinkedList Implementation");
        System.out.println(" 4 ---> Set Implementation");
        System.out.println("5----->JDBC Implementation");
        System.out.println(" ***********enter input*************** \n");

        Scanner sn=new Scanner(System.in);
        int option=sn.nextInt();
        
        sn.close();
        
        SavingsAccountDAO dao=SavingsAccountDAOFactory.getDaofactory(option);
        SavingsAccountService service=SavingsAccountServiceFactory.serviceFactory(dao);
    
    	SavingsAccountController controller=new SavingsAccountController(service);
    	
    	Address ashishAddress= new Address("bhopal","MP",132998);
    	
            SavingsAccount ashishaccount= controller.createSavingsAccount("ashish", 50_000,ashishAddress);

            SavingsAccount riteshaccount= controller.createSavingsAccount("ritesh", 60_000);
            
        
        System.out.println("account Id :" +ashishaccount.getAccountNumber());
        System.out.println("account name  :" +ashishaccount.getCustomerName());
        System.out.println("account Id :" +ashishaccount.getAccountBalance());

        System.out.println("account Id :" +riteshaccount.getAccountNumber());
        System.out.println("account name  :" +riteshaccount.getCustomerName());
        System.out.println("account Id :" +riteshaccount.getAccountBalance());
        try {
        controller.transfer(ashishaccount.getAccountNumber(),15,5_000);
        }catch(InsufficientBalanceException exception) {
        	System.out.println(exception.getMessage());
        }catch(AccountNotFoundException ex) {
        	System.out.println(ex.getMessage());
        }

        System.out.println("account Id :" +riteshaccount.getAccountNumber());
        System.out.println("account name  :" +riteshaccount.getCustomerName());
        System.out.println("account Id :" +riteshaccount.getAccountBalance());
	}
	

}