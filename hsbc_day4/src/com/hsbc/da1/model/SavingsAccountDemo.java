package com.hsbc.da1.model;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SavingsAccountDemo {
	public static void main(String[] args) {
		
		SavingsAccount ravi=new SavingsAccount("Ravi", 10_000);
		SavingsAccount binod=new SavingsAccount("Binod", 1_00_000);
//		ravi.setAccountNumber(12345);
//		binod.setAccountNumber(1234567);
//		
		Set<SavingsAccount> set=new HashSet<>();
		
		set.add(ravi);
		set.add(binod);
		
		Iterator<SavingsAccount> it=set.iterator();
		
		while(it.hasNext()) {
			System.out.println(it.next().toString());
			
		}
		
		
	}

}
