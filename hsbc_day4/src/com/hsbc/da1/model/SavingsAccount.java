package com.hsbc.da1.model;

public class SavingsAccount {
	private String customerName;
	
	private long accountNumber;
	
	private double accountBalance;
	private static long counter=1000;
	private Address address;
	
	public SavingsAccount(String customerName, double accountBalance) {
		this.customerName=customerName;
		this.accountBalance=accountBalance;
		this.accountNumber=++counter;
		
	}
	public SavingsAccount(String customerName, double accountBalance,Address address) {
		this.customerName=customerName;
		this.accountBalance=accountBalance;
		this.address=address;
		this.accountNumber=++counter;
		
	}


	public String getCustomerName() {
		return customerName;
	}
	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public long getAccountNumber() {
		return accountNumber;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (accountNumber ^ (accountNumber >>> 32));
		result = prime * result + ((customerName == null) ? 0 : customerName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof SavingsAccount)) {
			return false;
		}
		SavingsAccount other = (SavingsAccount) obj;
		if (accountNumber != other.accountNumber) {
			return false;
		}
		if (customerName == null) {
			if (other.customerName != null) {
				return false;
			}
		} else if (!customerName.equals(other.customerName)) {
			return false;
		}
		return true;
	}
	
	public String toString() {
		return "SavingsAccount [customerName=" + customerName + ", accountNumber=" + accountNumber + ", accountBalance="
				+ accountBalance + "]";
	}

	}
	

