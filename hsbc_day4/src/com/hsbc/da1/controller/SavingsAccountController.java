package com.hsbc.da1.controller;

import com.hsbc.da1.model.SavingsAccount;
import com.hsbc.da1.service.SavingsAccountService;
import com.hsbc.da1.service.SavingsAccountServiceImpl;
import com.hsbc.da1.util.SavingsAccountServiceFactory;

import java.util.Collection;
import java.util.List;

import javax.security.auth.login.AccountNotFoundException;

import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.Address;

public class SavingsAccountController {
	
	private SavingsAccountService savingAccountService;
	
	public SavingsAccountController(SavingsAccountService savingsAccountService) {
		this.savingAccountService=savingsAccountService;
	}
	
	public SavingsAccount createSavingsAccount(String customerName,double accountBalance) {
		
		return this.savingAccountService.createSavingsAccount(customerName, accountBalance);
	  
		
		
	}

	public SavingsAccount createSavingsAccount(String customerName,double accountBalance,Address address) {
		
		return this.savingAccountService.createSavingsAccount(customerName, accountBalance, address);
  
		
	
}

	public double withdraw(long accountId,double amount)throws InsufficientBalanceException ,AccountNotFoundException{
		return this.savingAccountService.withdraw(accountId, amount);
		
		
	}
	public void transfer(long senderId,long reciverId,double amount) throws InsufficientBalanceException ,AccountNotFoundException{
		this.savingAccountService.transfer(senderId,reciverId,amount);
	}
	

	public double deposit(long accountId,double amount) throws AccountNotFoundException{
		return this.savingAccountService.deposit(accountId, amount);
		

	
	}
public SavingsAccount fetchAccountById(long accountId) throws AccountNotFoundException{
	return this.savingAccountService.fetchAccountById(accountId);
		
		
		
	}
	
public Collection<SavingsAccount> fetchAllAccounts() {
	
	
	return this.savingAccountService.fetchAllAccounts();
}


}