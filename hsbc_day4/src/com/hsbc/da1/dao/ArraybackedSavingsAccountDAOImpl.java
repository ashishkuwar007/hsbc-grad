package com.hsbc.da1.dao;

import java.util.Arrays;
import java.util.List;

import javax.security.auth.login.AccountNotFoundException;

import com.hsbc.da1.model.SavingsAccount;

public class ArraybackedSavingsAccountDAOImpl implements SavingsAccountDAO{
	private static SavingsAccount[] savingsAccounts=new SavingsAccount[100];
	private static int counter;
	
	public SavingsAccount createSavingsAccount(SavingsAccount savingsAccount) {
		savingsAccounts[counter++]=savingsAccount;
		return savingsAccount;
		
		
	}
	public List <SavingsAccount> fetchAllAccounts() {
		return Arrays.asList(savingsAccounts);		
		
	}
	public SavingsAccount fetchAccountById(long accountId)throws AccountNotFoundException {
		
		for(int index=0;index<savingsAccounts.length;index++) {
			if(savingsAccounts[index]!=null && savingsAccounts[index].getAccountNumber()==accountId) {
				return savingsAccounts[index];
			}
		}
		throw new  AccountNotFoundException("account not found");
	}
	
	
	public void updateSavingsAccount(long accountId,SavingsAccount savingsAccount) {
		for(int index=0;index<savingsAccounts.length;index++) {
			if(savingsAccounts[index].getAccountNumber()==accountId) {
				
				savingsAccounts[index]=savingsAccount;
				break;
			}
			
			
		}
		return;
		
		
	}
	
	public void deleteSavingsAccount(long accountNumber) {
		for(int index=0;index<savingsAccounts.length;index++) {
			if(savingsAccounts[index].getAccountNumber()==accountNumber) {
				savingsAccounts[index]=null;
			}
			break;
		}
		
	}

	

}