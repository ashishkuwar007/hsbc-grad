package com.hsbc.da1.dao;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.security.auth.login.AccountNotFoundException;

import com.hsbc.da1.model.SavingsAccount;

public class SetbackedSavingsAccountDAOImpl implements SavingsAccountDAO {
	private static Set<SavingsAccount> set=new HashSet<>();

	@Override
	public SavingsAccount createSavingsAccount(SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		set.add(savingsAccount);
		return savingsAccount;
	}

	@Override
	public Collection<SavingsAccount> fetchAllAccounts() {
		// TODO Auto-generated method stub
		return set;
	}

	@Override
	public SavingsAccount fetchAccountById(long accountId) throws AccountNotFoundException {
		// TODO Auto-generated method stub
		Iterator<SavingsAccount> it=set.iterator();
	int value=0;
	
	
		while(it.hasNext()) {
			
			SavingsAccount savingsAccount=it.next();
			if(savingsAccount.getAccountNumber()==accountId) {
			return savingsAccount;
			}
		}
		throw new AccountNotFoundException("Account not found");
	}

	@Override
	public void updateSavingsAccount(long accountId, SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		Iterator<SavingsAccount> it=set.iterator();
		while(it.hasNext()) {
			SavingsAccount updated=it.next();
			if(updated.getAccountNumber()==accountId) {
			updated=savingsAccount;
			}
		}
		
	}

	@Override
	public void deleteSavingsAccount(long accountNumber) {
		// TODO Auto-generated method stub
		Iterator<SavingsAccount> it=set.iterator();
		while(it.hasNext()) {
			SavingsAccount updated=it.next();
			if(it.next().getAccountNumber()==accountNumber) {
			set.remove(it.next());
			}
		}
		
		
	}

}
