package com.hsbc.da1.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.security.auth.login.AccountNotFoundException;

import com.hsbc.da1.model.SavingsAccount;

public class JdbcBackedDAOImpl implements SavingsAccountDAO {

	private static final  String url="jdbc:derby://localhost:1527/items";
	private static final String username="ashish";
	private static final  String password="password";
	
	

	private static final String INSERT_QUERY = "insert into savings_account (account_number, cust_name, account_balance)"
			+ " values ";

	private static final String SELECT_QUERY = "select * from savings_account";

	private static final String DELETE_BY_ID_QUERY = "delete  from savings_account where account_number=";
	
	private static final String SELECT_BY_ID_QUERY = "select *  from savings_account where account_number=";
	private static final String UPDATE_QUERY = "Update savings_account set  account_number =" +  "cust_name ="+" account_balance=" +"from savings_account";

	private static Statement getStatement() {
	try {Connection connection = DriverManager.getConnection(url,username,password);
			return connection.createStatement();} 
		
		// count = stmt.executeUpdate("insert into items (name, price) values ('iPhone',
		// 45000)");
		// System.out.println("Number of items inserted :: " + count);

	catch (SQLException e) {
		e.printStackTrace();

	}
	return null;
	}


	@Override
	public SavingsAccount createSavingsAccount(SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		String query = INSERT_QUERY + " (" + savingsAccount.getAccountNumber() + ", " +"'"+ savingsAccount.getCustomerName()+"'"
				+ "," + savingsAccount.getAccountBalance() + ")";
		System.out.println("query submitt ed to the db :  "+ query);
		int rowsUpdated = 0;
		try {
			rowsUpdated = getStatement().executeUpdate(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (rowsUpdated == 1) {
			return savingsAccount;
		}
		return null;

	}

	@Override
	public Collection<SavingsAccount> fetchAllAccounts() {
		// TODO Auto-generated method stub
		
		String query = SELECT_QUERY;
		Set<SavingsAccount> savingsAccountSet = new HashSet<>();
		try {
			ResultSet rs = getStatement().executeQuery(query);
			if ( rs.next()) {
				SavingsAccount savingsAccount = 
						new SavingsAccount(rs.getString("cust_name"), rs.getDouble("account_balance"));
				savingsAccountSet.add(savingsAccount);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return savingsAccountSet;

	}

	@Override
	public SavingsAccount fetchAccountById(long accountId) throws AccountNotFoundException {
		// TODO Auto-generated method stub
		String query = SELECT_BY_ID_QUERY+ " "+ accountId;
		try {
			ResultSet rs = getStatement().executeQuery(query);
			if ( rs.next()) {
				SavingsAccount savingsAccount = 
						new SavingsAccount(rs.getString("cust_name"), rs.getDouble("account_balance"));
				return savingsAccount;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new AccountNotFoundException(" Customer with "+ accountId+ " does not exists");
		}
		return null;

	}

	@Override
	public void updateSavingsAccount(long accountId, SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		String query = "Update savings_account set  cust_name ="+savingsAccount.getCustomerName()+ ",account_balance= "+savingsAccount.getAccountBalance()+"from savings_account where account_number = "+accountId;
		
		try {
		getStatement().executeUpdate(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	@Override
	public void deleteSavingsAccount(long accountNumber) {
		// TODO Auto-generated method stub
		String query = DELETE_BY_ID_QUERY + ""+accountNumber;
		
		try {
			getStatement().executeUpdate(query);
		}catch (SQLException e) {
			// TODO Auto-generated catch block
				e.printStackTrace();
		}
		
	}

}
