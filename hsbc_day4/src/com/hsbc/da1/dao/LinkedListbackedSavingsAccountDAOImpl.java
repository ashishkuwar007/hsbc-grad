package com.hsbc.da1.dao;

import java.util.LinkedList;
import java.util.List;

import javax.security.auth.login.AccountNotFoundException;

import com.hsbc.da1.model.SavingsAccount;

public class LinkedListbackedSavingsAccountDAOImpl implements SavingsAccountDAO {
	private static LinkedList<SavingsAccount> sa=new LinkedList<>();

	@Override
	public SavingsAccount createSavingsAccount(SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		sa.add(savingsAccount);
		
		return savingsAccount;
	}

	@Override
	public List<SavingsAccount> fetchAllAccounts() {
		// TODO Auto-generated method stub
		return sa;
	}

	@Override
	public SavingsAccount fetchAccountById(long accountId) throws AccountNotFoundException {
		for(SavingsAccount savingsAccount :sa) {
			if(savingsAccount.getAccountNumber()==accountId) {
				return savingsAccount;
			}
		}
		// TODO Auto-generated method stub
		throw new AccountNotFoundException("Account not found");
	}

	@Override
	public void updateSavingsAccount(long accountId, SavingsAccount savingsAccount) {
		// TODO Auto-generated method stub
		for(SavingsAccount savings:sa) {
			if(savings.getAccountNumber()==accountId) {
				savings=savingsAccount;
			}
		}
		
		
		
	}

	@Override
	public void deleteSavingsAccount(long accountNumber) {
		// TODO Auto-generated method stub
		for(SavingsAccount savingsAccount :sa) {
			if(savingsAccount.getAccountNumber()==accountNumber) {
				sa.remove(savingsAccount);
			}
		}
	}
	

}
