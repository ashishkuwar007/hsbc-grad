package com.hsbc.da1.dao;
import java.util.Collection;
import java.util.List;

import javax.security.auth.login.AccountNotFoundException;

import com.hsbc.da1.model.SavingsAccount;

public interface SavingsAccountDAO {
	
	public SavingsAccount createSavingsAccount(SavingsAccount savingsAccount);
	
	public Collection<SavingsAccount> fetchAllAccounts();
	
	public SavingsAccount fetchAccountById(long accountId) throws AccountNotFoundException;
		
	
	
	
	public void updateSavingsAccount(long accountId,SavingsAccount savingsAccount);
	
	public void deleteSavingsAccount(long accountNumber);
	
	
}