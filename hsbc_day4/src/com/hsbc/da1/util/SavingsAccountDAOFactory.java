package com.hsbc.da1.util;

import com.hsbc.da1.dao.ArraybackedSavingsAccountDAOImpl;
import com.hsbc.da1.dao.JdbcBackedDAOImpl;
import com.hsbc.da1.dao.LinkedListbackedSavingsAccountDAOImpl;
import com.hsbc.da1.dao.ListbackedSavingsAccountDAOImpl;
import com.hsbc.da1.dao.SavingsAccountDAO;
import com.hsbc.da1.dao.SetbackedSavingsAccountDAOImpl;

public class SavingsAccountDAOFactory {
	
	
	public static SavingsAccountDAO getDaofactory(int value) {
		
		switch(value) {
		case(1):
			return new ArraybackedSavingsAccountDAOImpl();
		case(2):
			return new ListbackedSavingsAccountDAOImpl();
		case(3):
			return new LinkedListbackedSavingsAccountDAOImpl();
		case(4):
			return new SetbackedSavingsAccountDAOImpl();
		default :
			return new JdbcBackedDAOImpl();
		
		
	}

}
}