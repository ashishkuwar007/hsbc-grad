package hsbc-traning.hsbc-day1;

public class Matrix {

    static value = 10;

    public static void main(String[] args) {

       
        int[][] matrix = new int[5][5];

        assignMatrix(matrix);
        printMatrix(matrix);

    }

    public static void assignMatrix(int[][] matrix) {
        for (int row = 0; row < 5; row++) {
            for (int col= 0; col < 5; col++) {
                matrix [row][col]=value++;
            }
        }
    }

    public static void printMatrix(int[][] matrix) {
        for (int row = 0; row < 5; row++) {
            for (int col= 0; col< 5; col++) {
                System.out.print("\t" + matrix [row][col] + " \t");
            }
            System.out.println();
        }
    }
}

    
}
